# MongoDB LSV

Made by Ronald Paternina Castro

### Requeriments

 - Docker
 - docker-compose

 - **Iniciar el Mongo:**
```sh
$ git clone https://gitlab.com/valentinc94/mongodb-curso-lsv.git
$ docker-compose up
```

 - **Contenedor docker:**
```sh
$ docker exec -it docker_id /bin/bash
```


```sh
$ mongo
```
